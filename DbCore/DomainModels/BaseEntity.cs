﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCore.DomainModels
{
    public abstract class BaseEntity<T>
    {
        [Key]
        [BsonId]
        public T Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}

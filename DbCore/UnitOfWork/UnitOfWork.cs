﻿using DbCore.DomainModels;
using DbCore.Interfaces;
using DbCore.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCore.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IMongoContext _mongoContext;
        private BaseRepository<User> _userRepository;

        public UnitOfWork(IMongoContext mongoContext)
        {
            _mongoContext = mongoContext;
        }

        public BaseRepository<User> Users
        {
            get
            {
                if (_userRepository == null)
                    _userRepository = new BaseRepository<User>(_mongoContext);
                return _userRepository;
            }
        }
        public async Task<bool> Commit()
        {
            var result = await _mongoContext.SaveChange();

            return result > 0;
        }

        public void Dispose()
        {
            _mongoContext.Dispose();
        }
    }
}

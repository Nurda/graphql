﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCore.Interfaces
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        Task Add(TEntity entity);
        Task AddRange(IEnumerable<TEntity> entities);
        Task<TEntity> GetById(Guid id);
        Task<IEnumerable<TEntity>> GetAll();
        //Task<IQueryable<TEntity>> Get(Func<>)
        Task Update(TEntity entity);
        Task UpdateRange(IEnumerable<TEntity> entities);
        Task<bool> Remove(Guid id);
        Task RemoveRange(IEnumerable<Guid> ids);
        IQueryable<TEntity> AsQueryable();
    }
}

﻿using DbCore.DomainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCore.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}

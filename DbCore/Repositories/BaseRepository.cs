﻿using DbCore.DomainModels;
using DbCore.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCore.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : MongoEntity
    {
        protected readonly IMongoContext _context;
        protected IMongoCollection<TEntity> _dbSet;

        public BaseRepository(IMongoContext context)
        {
            _context = context;
            _dbSet = _context.GetCollection<TEntity>(typeof(TEntity).Name);
        }

        public virtual async Task Add(TEntity entity)
        {
            await _dbSet.InsertOneAsync(entity);
        }

        public virtual async Task AddRange(IEnumerable<TEntity> entities)
        {
            await _dbSet.InsertManyAsync(entities);
        }

        public virtual async Task<TEntity> GetById(Guid id)
        {
            var result = await _dbSet.FindAsync(Builders<TEntity>.Filter.Eq("_id", id));
            return result.FirstOrDefault();
        }

        public virtual async Task<IEnumerable<TEntity>> GetAll()
        {
            var result = await _dbSet.FindAsync(Builders<TEntity>.Filter.Empty);
            return result.ToList();
        }

        public virtual async Task Update(TEntity entity)
        {
            await _dbSet.ReplaceOneAsync(Builders<TEntity>.Filter.Eq("_id", entity.Id), entity);
        }

        public virtual async Task UpdateRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities) 
                await _dbSet.ReplaceOneAsync(Builders<TEntity>.Filter.Eq("_id", entity.Id), entity);
        }

        public virtual async Task<bool> Remove(Guid id)
        {
            var result = await _dbSet.DeleteOneAsync(Builders<TEntity>.Filter.Eq("_id", id));
            return result.IsAcknowledged;
        }

        public virtual async Task RemoveRange(IEnumerable<Guid> ids)
        {
            foreach (var id in ids)
                await _dbSet.FindOneAndDeleteAsync(Builders<TEntity>.Filter.Eq("_id", id));
        }

        public virtual void Dispose()
        {
            _context?.Dispose();
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return _dbSet.AsQueryable();
        }
    }
}

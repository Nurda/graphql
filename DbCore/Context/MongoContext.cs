﻿using DbCore.Interfaces;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbCore.Context
{
    public class MongoContext : IMongoContext
    {
        private IMongoDatabase _database;
        public IClientSessionHandle session;
        public MongoClient mongoClient;
        private readonly List<Func<Task>> _commands;
        private readonly IConfiguration _configuration;

        public MongoContext(IConfiguration configuration)
        {
            _configuration = configuration;
            _commands = new List<Func<Task>>();
            mongoClient = new MongoClient(_configuration["MongoSettings:Connection"]);
            _database = mongoClient.GetDatabase(_configuration["MongoSettings:DatabaseName"]);
        }

        public async Task<int> SaveChange()
        {
            using (session = await mongoClient.StartSessionAsync())
            {
                session.StartTransaction();

                var commandTasks = _commands.Select(x => x());
                await Task.WhenAll(commandTasks);
                await session.CommitTransactionAsync();
            }

            return _commands.Count;
        }

        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _database.GetCollection<T>(name);
        }
        public void Dispose()
        {
            session?.Dispose();
            GC.SuppressFinalize(this);
        }
        public void AddCommand(Func<Task> func)
        {
            _commands.Add(func);
        }
    }
}

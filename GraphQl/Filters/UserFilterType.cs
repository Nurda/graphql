﻿using DbCore.DomainModels;
using HotChocolate.Data.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQl.Filters
{
    public class UserFilterType : FilterInputType<User>
    {
        protected override void Configure(
        IFilterInputTypeDescriptor<User> descriptor)
        {
            descriptor.BindFieldsExplicitly();
            descriptor.Field(f => f.UserName);
            descriptor.Field(f => f.Id);
            descriptor.Field(f => f.CreatedDate);
        }
    }
}

﻿using DbCore.DomainModels;
using DbCore.Interfaces;
using DbCore.ViewModel;
using GraphQl.Filters;
using HotChocolate;
using HotChocolate.Data;
using HotChocolate.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQl.Query
{
    public class UserQuery
    {
        public async Task<IEnumerable<User>> Users([Service] IUserRepository _userRepository)
        {
            return await _userRepository.GetAll();
        }

        public async Task<User> GetUserById([Service] IUserRepository _userRepository, Guid id)
        {
            return await _userRepository.GetById(id);
        }

        [UseFiltering(typeof(UserFilterType))]
        public IQueryable<User> GetUserByFiltering([Service] IUserRepository _userRepository)
        {
            return _userRepository.AsQueryable();
        }
    }
}

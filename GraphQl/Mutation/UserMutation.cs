﻿using DbCore.DomainModels;
using DbCore.Interfaces;
using DbCore.ViewModel;
using HotChocolate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GraphQl.Mutation
{
    public class UserMutation
    {
        IUnitOfWork _unitOfWork;
        public UserMutation(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public User AddUser([Service] IUserRepository _userRepository, UserCreate user)
        {
            var item = new User
            {
                CreatedDate = DateTime.Now,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Name = user.Name,
                UserName = user.UserName
            };

            _userRepository.Add(item);
            //var res = await _unitOfWork.Commit();

            return item;
        }
        public async Task<IEnumerable<User>> AddUsers([Service] IUserRepository _userRepository, IEnumerable<UserCreate> data)
        {
            List<User> users = new List<User>();

            foreach (var user in data)
            {
                users.Add(new User
                {
                    CreatedDate = DateTime.Now,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Name = user.Name,
                    UserName = user.UserName
                });
            }

            await _userRepository.AddRange(users);

            return users;
        }
        public async Task<User> UpdateUser([Service] IUserRepository _userRepository, UserUpdate data)
        {
            var user = await _userRepository.GetById(data.Id);

            user.FirstName = data.FirstName;
            user.LastName = data.LastName;
            user.Name = data.Name;
            user.UserName = data.UserName;
            user.IsDeleted = data.IsDeleted;

            await _userRepository.Update(user);

            return user;
        }
        public async Task<bool> DeleteUser([Service] IUserRepository _userRepository, Guid id)
        {
            return await _userRepository.Remove(id);
        }
    }
}
